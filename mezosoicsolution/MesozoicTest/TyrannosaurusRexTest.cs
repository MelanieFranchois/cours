﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;


namespace MesozoicTest
{
    [TestClass]
    public class TyrannosaurusRexTest
    {
        [TestMethod]
        public void TestTyrannosaurusRexConstructor()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("TyrannosaurusRex", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestTyrannosaurusRexRoar()
        {
            Dinosaur louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("COUNTRY ROADS, TAKE ME HOOOOOME", louis.roar());
        }

        [TestMethod]
        public void TestTyrannosaurusRexSayHello()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("Je suis Louis, le trex, j'ai 12 ans pepehands", louis.sayHello());
        }

        [TestMethod]
        public void TestTyrannosaurusRexHug()
            {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Dinosaur nessie = new TyrannosaurusRex("Nessie", 12);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie", louis.hug(nessie));

        }
    }
}
