﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;


namespace MesozoicTest
{
    [TestClass]
    public class DiplodocusTest
    {
        [TestMethod]
        public void TestDiplodocusConstructor()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Diplodocus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDiplodocusRoar()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual("lbblbllbbl", louis.roar());
        }

        [TestMethod]
        public void TestDiplodocusSayHello()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual("Je suis Louis, le Diplo, j'ai 12 ans wsh", louis.sayHello());
        }

        [TestMethod]
        public void TestDiplodocusHug()
            {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Diplodocus nessie = new Diplodocus("Nessie", 12);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie", louis.hug(nessie));

        }
    }
}
