﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;


namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Dinosaur Jakcy = new TyrannosaurusRex("Jacky", 15);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
            {
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Dinosaur nessie = new Stegausaurus("Nessie", 12);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie", louis.hug(nessie));

        }
    }
}
