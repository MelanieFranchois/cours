﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net; //pour utiliser DNS.GetHostEntry

namespace TD1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_quit_Click(object sender, RoutedEventArgs e)
        {
            //executé lors d'un clic sur le bouton "quitter"
            MessageBox.Show("ByeBye");
            Application.Current.Shutdown(); //pour quitter la page utiliser shutdown
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            //executé lors d'un clic sur le bouton "ok"
            IPHostEntry ihe; //déclare l'objet
                             // ihe = Dns.GetHostEntry("www.iut-amiens.fr"); //affecte à l'objet (ihe) et affiche
                             // MessageBox.Show(ihe.HostName); //on affiche l'attibut hostname, il y aura le nom officiel de l'objet dans l'attribut

            try //on y met que des informations qui peuvent échouer
            {
                ihe = Dns.GetHostEntry(textbox_saisie.Text);
                //MessageBox.Show(e.ToString());
                //return;
            }

            catch(Exception ee) //en cas d'echec
            {
                //executé seulement si echec
                // MessageBox.Show(ee.ToString()); //converti l'objet en une chaine grace a tostring
                MessageBox.Show(ee.Message);//converti l'objet en message
                return; //message d'erreur donc on quitte avec return
            }
            //ici tout va bien
            list_box.Items.Clear();
            list_box.Items.Add("Machine : " + ihe.HostName); //Pour afficher dans la liste box
            list_box.Items.Add("\nAdresses IP : ");
            // un tableau est une collection
            foreach (IPAddress ip in ihe.AddressList)//ihe.addresselist est le nom du tableau, IPAdresse est la variable qui aura la valeur du tableau pointé
                list_box.Items.Add(ip.ToString());           
        }
    }
}
